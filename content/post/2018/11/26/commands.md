---
title: "常用命令"
date: 2018-11-23T11:11:11+08:00
draft: false
---

# Hugo搭建个人博客本人常用命令

```java
编译：hugo --theme=coder-portfolio --baseUrl="https://nghsky.github.io"

cd public
git init
git remote add origin http://github.com/nghsky/nghsky.github.io.git
git add -A
git commit -m "first commit"
git push -u origin master
```

### git常用命令
```java
git status	
git add -A		添加多文件
git add . 		提交新添加和被修改的文件，不包含被删除的
git add -u 		提交被修改和被删除的文件，不提交新添加的文件
git commit -m "本次提交说明"
git push
```


## GIT Command line instructions
#### Git global setup
```java
git config --global user.name "吴昊天"
git config --global user.email "nghsky@foxmail.com"
```

#### Create a new repository
```java
git clone git@gitlab.com:vmsWHT/vms-wx.git	  
cd FsaMonitor
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```

#### Existing folder
```java
cd existing_folder
git init
git remote add origin git@gitlab.com:vmsWHT/vms-wx.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

#### Existing Git repository
```java
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:vmsWHT/vms-wx.git
git push -u origin --all
git push -u origin --tags
```

---

# 致谢
感谢您花时间阅读，谢谢。