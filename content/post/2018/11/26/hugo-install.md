---
title: "安装Hugo"
date: 2018-11-26T15:23:35+08:00
draft: false
---

# Hugo 官方主页：https://gohugo.io 


# 方法一

### 安装Chocolatey

 - 以管理员身份运行cmd命令行
 ```
 @"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
 ```
 - 成功后，输入choco，会显示Chocolatey的版本
 - 在安装好choco后，cmd下执行：
 - choco install hugo -confirm （官网方法）
 - 成功后，执行hugo version。会显示hugo版本号。如果失败查看一下环境变量是否适配

 
# 方法二

 - 从 hugo 的 GitHub 仓库 下载安装包(https://github.com/gohugoio/hugo/releases)，使用即可。下载解压后添加到 Windows 的系统环境变量的 PATH 中即可，不需安装。
 
### 初始化项目 
 - C:\Users\WuHaotian>hugo new site blog
 命令执行后创建项目目录，其中的目录结构为：
 - ├─archetypes
 - ├─content
 - ├─data
 - ├─layouts
 - ├─static
 - ├─themes
 - └─config.toml
 - config.toml 是网站的配置文件，hugo 同时还支持 YAML 格式的 config.yaml 或 JSON 格式的 config.json。content 目录放 markdown 文章，data 目录放数据，layouts 目录放网站模板文件，static 目录放图片、css、js 等静态资源，themes 命令放下载的主题。


### 下载主题
 - hugo 默认不带主题，但是没有主题的话又无法工作。所以，请在 主题网站 选择你看中的主题后，点击下载链接从 GitHub 下载到 themes 目录中。(https://themes.gohugo.io/)
 - C:\Users\WuHaotian\blog>cd themes
 - C:\Users\WuHaotian\blog\themes>git clone https://github.com/luizdepra/hugo-coder.git （why？coder==）
 - 检查 themes 目录下是否成功下载主题：
 - C:\Users\WuHaotian\blog\themes>tree
	 文件夹 PATH 列表
	卷序列号为 00000049 30BD:DD70
	C:.
	- └─hugo-bootstrap
	- 	├─exampleSite
	- 	│  ├─content
	- 	│  │  └─post
	-	│  └─layouts
	-	│      └─partials
	-	├─i18n
	-	├─images
	-	├─layouts
	-	│  ├─partials
	-	│  └─_default
	-	└─static
	-		└─css
 - 
 
### 添加内容

 - 在项目目录中执行 hugo new XX.md 命令，会在 content 目录中创建这个 XX.md 文件。
 hugo new about.md
 
 - 这个文件的默认内容如下：

	```
	title: "About"
	date: 2018-05-22T22:04:26+08:00
	draft: true
	```

 - 文件默认内容在，draft 表示是否是草稿，编辑完成后请将其改为 false，否则编译会跳过草稿文件。具体的博客内容在下面写：
 - 内容随便写
 
 - 对于博客文件，通常放在 content/post 目录中：
 
### 启动 hugo 自带的服务器
 - 在项目根目录下，通过 hugo server 命令可以使用 hugo 内置服务器调试预览博客。--theme 选项可以指定主题，--watch 选项可以在修改文件后自动刷新浏览器，--buildDrafts 包括标记为草稿（draft）的内容。
 - C:\Users\WuHaotian\blog>hugo server --theme=coder-portfolio --buildDrafts --watch
 - 启动成功后在网页输入 http://localhost:1313（首页），http://localhost:1313/about（about页面）

### 生成public
 - 最终我们需要的是静态的 HTML 文件。--theme 选项指定主题，--baseUrl 指定了项目的网站。最终静态文件生成在 public 目录中：
 - C:\Users\WuHaotian\blog>hugo --theme=coder-portfolio --baseUrl="https://nghsky.github.io" （baseurl中要使用https，http会导致不能文件访问）
 
---

# 致谢
感谢您花时间阅读，谢谢。