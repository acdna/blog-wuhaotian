---
title: "Win10安装docker"
date: 2018-11-26T10:30:35+08:00
draft: false
---

# 目前Docker提供专门的Win10专业版系统的安装包，首先需要开启Hyper-V

### 开启Hyper-V

 - 程序和功能-->启用或关闭Windows功能-->勾选 Hyper-V

# 安装Docker

 - 最新版 Toolbox 下载地址： https://www.docker.com/get-docker
 - 下载时需要登录docker，注册时注册doker id不能点击Sign Up 按钮(禁用)，网页有一个 google 的人机验证按钮，需要使用代理
 - 运行安装文件，双击下载的 Docker for Windows Installe 安装文件，一路 Next，点击 Finish 完成安装
 - 安装完成后，Docker 会自动启动。通知栏上会出现个小鲸鱼的图标 ，这表示 Docker 正在运行。
 - 桌边也会出现三个图标，我们可以在命令行执行 docker version 来查看版本号，docker run hello-world 来载入测试镜像测试

# 更改docker拉取的镜像存储路径

 - pull镜像的默认路径为：C:\Users\Public\Documents\Shared Virtual Machines
 - 首先需要把路径下的映像文件复制到你要改的路径，比如我的：D:\vmwork\Virtual hard disks
 - 打开控制面板->管理工具->打开Hyper-v管理器->右击->Hyper-v设置
 - 将虚拟硬盘文件路径更改为自己设置的

 
# 镜像加速

 - 鉴于国内网络问题，后续拉取 Docker 镜像十分缓慢，我们可以需要配置加速器来解决，我使用的是网易的镜像地址：http://hub-mirror.c.163.com
 - 新版的 Docker 使用 /etc/docker/daemon.json（Linux） 或者 %programdata%\docker\config\daemon.json（Windows） 来配置 Daemon
 - 请在该配置文件中加入（没有该文件的话，请先建一个）：
 ```
	{
		"registry-mirrors": ["http://hub-mirror.c.163.com"]
	}
 ```
 - 
 - Daemon：Docker for windows10 可以配置阿里云镜像，到https://cr.console.aliyun.com/ 注册一个账户，登录进去后再列表选择加速器，把你的专属加速器地址复制粘贴到Daemon的Registry mirrors中 


---

# 致谢
感谢您花时间阅读，谢谢。