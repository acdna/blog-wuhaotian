---
title: "List列表去重"
date: 2018-12-14T13:17:45+08:00
draft: false
keywords: ["list","repeat"]
description: "List列表去重"
tags: ["repeat"]
categories: ["repeat"]
author: "nghsky"
---


# Hugo搭建个人博客本人常用命令

```java
	List list = new ArrayList<>();
	if (CollectionUtils.isNotEmpty(oldList)) {
		for (Obj obj : oldList) {
			// list去重复，内部重写equals
			if (!list.contains(obj)) {
				list.add(obj);
			}
		}
	}
	
	
     /**
     * 重写equals,用于比较对象属性是否包含
     */
    public boolean equals(Object obj) {  
        if (obj == null) {  
            return false;  
        }  
        if (this == obj) {  
            return true;  
        }  
        User user = (User) obj;  
        //多重逻辑处理，去除年龄、姓名相同的记录
        if (this.getAge().compareTo(user.getAge())==0
                && this.getUserName().equals(user.getUserName())
                && this.getScore().compareTo(user.getScore())==0) {  
            return true;  
        }  
        return false;  
    }
```

---

# 致谢
感谢您花时间阅读，谢谢。

